﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Grindbot.Objects;
using GrindBot.Engine;

namespace GrindBot.Objects
{
    internal class _Engine
    {
        internal _Engine(List<State> parStates)
        {
            States = parStates;
            // Remember: We implemented the IComparer, and IComparable
            // interfaces on the State class!
        }

        internal List<State> States { get; }
        internal bool Running { get; }

        internal virtual string Pulse()
        {
            int then = Environment.TickCount;
            bool checkstate = false;
            string Name = "";
            // This starts at the highest priority state,
            // and iterates its way to the lowest priority.
            foreach (var state in States)
            {
                Name = state.Name;
                try
                {
                    /*
                    then = Environment.TickCount;

                    checkstate = state.NeedToRun;
                    if(checkstate)
                        state.Run();

                    Console.WriteLine("State: " + state.Name);
                    Console.WriteLine("Ran: " +checkstate);
                    Console.WriteLine("Took " + (Environment.TickCount - then) + "ms total.");
                    
                    if(checkstate)
                        return state.Name;
                    */
                    
                    if (state.NeedToRun)
                    {
                        state.Run();
                        return state.Name;
                        // Break out of the iteration,
                        // as we found a state that has run.
                        // We don't want to run any more states
                        // this time around.
                    }
                    
                }
                catch(Exception e)
                {
                    MessageBox.Show(Name);
                    Manager.StopCurrentEngine();
                    return "";
                }

            }
            return "";
        }
    }
}
