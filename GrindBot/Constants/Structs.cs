﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZzukBot.Helpers.PPather;

namespace GrindBot.Constants
{
    class Structs
    {
        //Should be in objects really :/
        internal class NPC
        {
            internal NPC(string parName, Location parPos, string parMapPos)
            {
                Name = parName;
                Coordinates = parPos;
                MapPosition = parMapPos;
            }

            internal string Name { get; private set; }
            internal Location Coordinates { get; private set; }
            internal string MapPosition { get; private set; }
        }

        internal struct RestockItem
        {
            internal string Item;
            internal int RestockUpTo;

            internal RestockItem(string parItem, int parRestockUpTo)
            {
                Item = parItem;
                RestockUpTo = parRestockUpTo;
            }
        }
    }
}
