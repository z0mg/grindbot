﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GrindBot.Engine.Grind;
using GrindBot.Engine.ProfileCreation;
using GrindBot.Gui;
using GrindBot.Settings;
using ZzukBot.Mem;

namespace GrindBot.Engine
{
    internal enum Engines
    {
        None = 0,
        ProfileCreation = 1,
        Grind = 2
    }

    internal static class Manager
    {
        private static object _Engine;

        private static Grinder tmpGrind;

        private static bool IsEngineRunning => _Engine != null;

        internal static Engines CurrentEngineType
        {
            get
            {
                if (IsEngineRunning)
                {
                    if (_Engine.GetType() == typeof(ProfileCreator))
                        return Engines.ProfileCreation;

                    if (_Engine.GetType() == typeof(Grinder))
                        return Engines.Grind;
                }
                return Engines.None;
            }
        }

        internal static T EngineAs<T>()
        {
            return (T)_Engine;
        }

        internal static void StartProfileCreation()
        {
            Main.MainForm.Invoke(new MethodInvoker(delegate
            {
                if (IsEngineRunning) return;
                _Engine = new ProfileCreator();
            }));
        }
        internal static void StartGrinder(bool parLoadLast)
        {
            string tmpProfileName = "";
            MainThread.Instance.Invoke((() =>
            {
                if (IsEngineRunning) return;
                
                if (parLoadLast && GrindSettings.Values.LastProfile != "")
                {
                    tmpProfileName = GrindSettings.Values.LastProfile;
                    GrindSettings.Values.Save();
                }
                else
                {
                    using (var locateProfile = new OpenFileDialog())
                    {
                        locateProfile.CheckFileExists = true;
                        locateProfile.CheckPathExists = true;
                        locateProfile.Filter = "xml Profile (*.xml)|*.xml";
                        locateProfile.FilterIndex = 1;
                        locateProfile.InitialDirectory = Paths.ProfileFolder;
                        locateProfile.ShowDialog();
                        if (locateProfile.FileName != null)
                        {
                            tmpProfileName = locateProfile.FileName;
                        }
                        else
                        {
                            return;
                        }
                    }


                }
            }));
            tmpGrind = new Grinder();
            if (tmpGrind.Prepare(tmpProfileName))
            {
                GrindSettings.Values.Load();
                GrindSettings.Values.LastProfile = tmpProfileName;
                Main.MainForm.lGrindLoadProfile.Text = "Profile: Loaded";
                tmpGrind.Run();
                _Engine = tmpGrind;

            }
        }



        internal static void StopCurrentEngine()
        {
            var dispose = true;
            if (!IsEngineRunning) return;
            if (_Engine.GetType() == typeof(ProfileCreator))
                dispose = EngineAs<ProfileCreator>().Dispose();

            if (_Engine.GetType() == typeof(Grinder))
            {
                EngineAs<Grinder>().Stop();
                tmpGrind = null;
            }

            if (dispose)
                _Engine = null;
        }
    }
}
