﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grindbot.Objects;
using ZzukBot.Helpers;
using ZzukBot.Game.Statics;
using ZzukBot.Objects;

namespace GrindBot.Engine.Grind.States
{
    internal class StateLoot : State
    {

        private int waitTimer = Environment.TickCount;

        private int lastCheck;
        private ulong oldMobGuid;
        private readonly Random ran = new Random();
        private int randomOpenLootDelay;
        private int randomTakeLootDelay;

        private WoWUnit mob = null;

        internal override int Priority => 36;

        internal override bool NeedToRun
        {
            get
            {
                if (mob != null && mob.CanBeLooted)
                    return true;

                //Check if we are waiting
                if (waitTimer > Environment.TickCount)
                    return false;

                //If we aren't waiting - see if we can find a lootable / skinablle mob
                var needToRun = Grinder.Access.Info.Loot.NeedToLoot && !Grinder.Access.Info.Vendor.GoBackToGrindAfterVendor
                                            && !Grinder.Access.Info.Vendor.TravelingToVendor;
                //If we can find no mob
                if (!needToRun)
                {
                    //Don't try again for 1.5 sec
                    waitTimer = Environment.TickCount + 1500;
                }
                //If we don't need to loot - reset mob search timer!
                if(!needToRun)
                    Wait.Remove("SearchTarget");
                //return true/false
                return needToRun;
            }
        } 

        internal override string Name => "Looting";

        internal override void Run()
        {
            mob = Grinder.Access.Info.Loot.LootableMob;
            if (mob == null) return;

            if (mob.Guid != oldMobGuid)
            {
                oldMobGuid = mob.Guid;
                lastCheck = Environment.TickCount;
                Wait.Remove("RunToLoot");
                Wait.Remove("Looting");
            }
            if (mob.Position.GetDistanceTo(ObjectManager.Instance.Player.Position) > 4.5f)
            {
                var tu = Grinder.Access.Info.PathToUnit.ToUnit(mob);
                if (tu.Item1)
                    ObjectManager.Instance.Player.CtmTo(tu.Item2);

                if (Environment.TickCount - lastCheck >= 5000)
                {
                    Wait.Remove("RunToLoot");
                }
                lastCheck = Environment.TickCount;

                if (Wait.For("RunToLoot", 10000))
                {
                    Grinder.Access.Info.Loot.AddToLootBlacklist(mob.Guid);
                }
                Wait.Remove("Looting");
                randomOpenLootDelay = ran.Next(250, 750) + Grinder.Access.Info.Latency;
                randomTakeLootDelay = ran.Next(50, 250) + Grinder.Access.Info.Latency;
            }
            else
            {
                if (!ZzukBot.Game.Frames.Loot.IsOpen)
                {
                    var auto = mob.IsSkinable;
                    if (Wait.For("LootClick", randomOpenLootDelay))
                        mob.Interact(true);
                }
                else
                {
                    if (Wait.For("LootTake12", randomTakeLootDelay))
                    {
                        ZzukBot.Game.Frames.Loot.Frame.LootAll();
                        if (ZzukBot.Game.Frames.Loot.Frame.LootCount == 0)
                            Grinder.Access.Info.Loot.AddToLootBlacklist(mob.Guid);
                        Wait.Remove("Looting");
                    }
                }
                if (Wait.For("Looting", 5000))
                {
                    Grinder.Access.Info.Loot.AddToLootBlacklist(mob.Guid);
                }
            }
        }
    }
}
