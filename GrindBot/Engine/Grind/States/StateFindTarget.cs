﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grindbot.Objects;
using GrindBot.Settings;
using ZzukBot.Helpers;
using ZzukBot.Game.Statics;

namespace GrindBot.Engine.Grind.States
{
    internal class StateFindTarget : State
    {
        internal override int Priority => 20;
        private int _timeStamp = Environment.TickCount;

        internal override bool NeedToRun
        {
            get
            {
                //if (Wait.For("SearchTarget", GrindSettings.Values.TargetSearchWait)) return false;
                if (Environment.TickCount < _timeStamp + GrindSettings.Values.TargetSearchWait) return false;
                _timeStamp = Environment.TickCount;
                return (!Grinder.Access.Info.Rest.NeedToDrink && !Grinder.Access.Info.Rest.NeedToEat);
            }
        }
                                            

        internal override string Name => "Find target";

        internal override void Run()
        {
            //Grinder.Access.Info.Target.SearchDirect = false;
            // Get the next best target
            var Next = Grinder.Access.Info.Target.NextTarget;
            if (Next == null) return;
            // target it if avaible
            ObjectManager.Instance.Player.SetTarget(Next);
        }
    }
}
