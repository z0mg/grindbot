﻿using ZzukBot.Game.Statics;
using Grindbot.Objects;

namespace GrindBot.Engine.Grind.States
{
    internal class StateLoadNextWaypoint : State
    {
        internal override int Priority => 14;

        internal override bool NeedToRun => Grinder.Access.Info.Waypoints.NeedToLoadNextWaypoint() && ObjectManager.Instance.Player.Casting == 0 &&
                                            ObjectManager.Instance.Player.Channeling == 0;

        internal override string Name => "Loading next Waypoint";

        internal override void Run()
        {
            Grinder.Access.Info.PathAfterFightToWaypoint.DisableAfterFightMovement();
            // load the next waypoint in line
            Grinder.Access.Info.Waypoints.LoadNextWaypoint();
            // face the waypoint and start moving
            ObjectManager.Instance.Player.CtmTo(Grinder.Access.Info.Waypoints.CurrentWaypoint);
        }
    }
}
