﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZzukBot.Game.Statics;
using Grindbot.Objects;

namespace GrindBot.Engine.Grind.States
{
    internal class StateWaitAfterFight : State
    {
        internal override int Priority => 43;

        internal override bool NeedToRun => Environment.TickCount - Grinder.Access.Info.Combat.LastFightTick <=
                                            Grinder.Access.Info.Latency * 2 + 450
                                            && !Grinder.Access.Info.Target.ShouldAttackTarget;

        internal override string Name => "Waiting";

        internal override void Run()
        {
            //Grinder.Access.Info.PathForceBackup.WeArrived();
            ObjectManager.Instance.Player.CtmStopMovement();
            // Nothing to do here
        }
    }
}
