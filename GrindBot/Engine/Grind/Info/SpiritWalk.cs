﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZzukBot.Game.Statics;

namespace GrindBot.Engine.Grind.Info
{
    internal class _SpiritWalk
    {
        internal bool ArrivedAtCorpse;
        internal bool GeneratePath;

        internal _SpiritWalk()
        {
            GeneratePath = true;
            ArrivedAtCorpse = false;
        }

        internal float DistanceToCorpse
        {
            get
            {
                return ObjectManager.Instance.Player.Position.GetDistanceTo2D(ObjectManager.Instance.Player.CorpsePosition);
            }
        } 
    }
}
