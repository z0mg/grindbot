﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrindBot.Constants;
using GrindBot.Settings;
using ZzukBot.Game.Statics;

namespace GrindBot.Engine.Grind.Info
{
    internal class _Vendor
    {
        internal bool GoBackToGrindAfterVendor = false;
        internal ZzukBot.Helpers.PPather.Path HotspotsToVendor;
        internal bool RegenerateSubPath = false;

        internal bool TravelingToVendor;

        internal _Vendor()
        {
            _NeedToVendor = false;
            TravelingToVendor = false;
        }

        private bool _NeedToVendor { get; set; }

        internal bool NeedToVendor
        {
            get
            {
                var res = Inventory.Instance.CountFreeSlots(false) < GrindSettings.Values.MinFreeSlotsBeforeVendor;
                if (res) _NeedToVendor = true;
                return _NeedToVendor;
            }
        }

        internal bool GossipOpen => (Lua.Instance.GetText(Strings.IsVendorOpen, Strings.GT_IsVendorOpen) == "true");

        internal void DoneVendoring()
        {
            _NeedToVendor = false;
        }
    }
}
