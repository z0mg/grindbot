﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GrindBot.Settings;
using ZzukBot.Game.Statics;
using ZzukBot.Helpers.PPather;
using ZzukBot.Objects;

namespace GrindBot.Engine.Grind.Info.Path
{
    internal class _PathToUnit
    {
        private ulong unitLastGuid;
        private IntPtr unitLastPtr;

        internal _PathToUnit()
        {
            unitLastPtr = IntPtr.Zero;
            unitLastGuid = 0;
        }

        private Location unitLastPos { get; set; }
        private int unitLastWaypointIndex { get; set; }
        private ZzukBot.Helpers.PPather.Path unitPath { get; set; }

        internal Tuple<bool, Location> ToUnit(WoWUnit parUnit)
        {
            if (ObjectManager.Instance.Player.Position.GetDistanceTo(parUnit.Position) < GrindSettings.Values.CtmStopDistance)
                return Tuple.Create(true, parUnit.Position);

            //Unsure what this is for
            if (parUnit.Guid != unitLastGuid ||
                //parUnit.Guid != unitLastPtr ||
                parUnit.Position.GetDistanceTo(unitLastPos) > 2f)//1.2f
            {
                unitPath = Navigation.Instance.CalculatePath(
                    ObjectManager.Instance.Player.Position,
                    parUnit.Position);
                unitLastWaypointIndex = 0;
                unitLastPos = parUnit.Position;
                unitLastGuid = parUnit.Guid;
                //unitLastPtr = parUnit.Pointer;
            }
            if (unitPath.Count() > 0)
            {
                if (Grinder.Access.Info.Waypoints.
                    NeedToLoadNextWaypoint(unitPath.Get(unitLastWaypointIndex)))
                {
                    var tmp = unitLastWaypointIndex + 1;
                    if (tmp != unitPath.Count())
                        unitLastWaypointIndex = tmp;
                }
            }
            var nextPoint = unitPath.Get(unitLastWaypointIndex);
            return Tuple.Create(true, nextPoint);
        }
    }
}
