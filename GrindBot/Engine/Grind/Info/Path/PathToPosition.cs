﻿using GrindBot.Settings;
using ZzukBot.Helpers;
using ZzukBot.Game.Statics;
using ZzukBot.Helpers.PPather;


namespace GrindBot.Engine.Grind.Info.Path
{
    internal class _PathToPosition
    {
        private Location posEndPos;
        private int posLastWaypointIndex;
        private ZzukBot.Helpers.PPather.Path posPath;

        internal Location ToPos(Location parPosition)
        {
            if (ObjectManager.Instance.Player.Position.GetDistanceTo(parPosition) < GrindSettings.Values.CtmStopDistance)
                return parPosition;

            var recalc = false;
            if (Wait.For("BetweenPoints", 5000))
            {
                recalc = !ToonBetweenPoints();
            }

            if (posEndPos == null || parPosition.GetDistanceTo(posEndPos) > 2f ||
                recalc)
            {
                posPath = Navigation.Instance.CalculatePath(ObjectManager.Instance.Player.Position,
                    parPosition);
                posLastWaypointIndex = 0;
                posEndPos = parPosition;
            }
            if (posPath.Count() > 0)
            {
                if (Grinder.Access.Info.Waypoints.
                    NeedToLoadNextWaypoint(posPath.Get(posLastWaypointIndex)))
                {
                    var tmp = posLastWaypointIndex + 1;
                    if (tmp != posPath.Count())
                        posLastWaypointIndex = tmp;
                }
                return posPath.Get(posLastWaypointIndex);
            }
            return posEndPos;
        }

        private bool ToonBetweenPoints()
        {
            if (posLastWaypointIndex == 0) return true;
            var PreWaypoint = posPath.Get(posLastWaypointIndex - 1);
            var Waypoint = posPath.Get(posLastWaypointIndex);
            var PlayerPos = ObjectManager.Instance.Player.Position;

            var DisAC = PreWaypoint.GetDistanceTo(Waypoint);
            var DisAB = PreWaypoint.GetDistanceTo(PlayerPos);
            var DisBC = PlayerPos.GetDistanceTo(Waypoint);
            var f = DisAB + DisBC;
            return f > DisAC - 1.5
                   && f < DisAC + 1.5;
        }
    }
}
