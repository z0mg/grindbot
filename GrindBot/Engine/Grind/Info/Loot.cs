﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GrindBot.Settings;
using ZzukBot.Objects;
using ZzukBot.Game.Statics;

namespace GrindBot.Engine.Grind.Info
{
    internal class _Loot
    {
        internal volatile bool BlacklistCurrentLoot;

        internal _Loot()
        {
            LootBlacklist = new List<ulong>();
        }

        private List<ulong> LootBlacklist { get; }

        internal WoWUnit LootableMob
        {
            get
            {
                var mobs = ObjectManager.Instance.Npcs;
                return mobs
                    .Where(i => (i.CanBeLooted ||
                                 (GrindSettings.Values.SkinUnits && i.IsSkinable && (GrindSettings.Values.NinjaSkin || i.TappedByMe))
                        )
                                && !LootBlacklist.Contains(i.Guid)
                                && !i.IsSwimming
                                && i.Position.GetDistanceTo(ObjectManager.Instance.Player.Position) < 32)
                    .OrderBy(i => i.Position.GetDistanceTo(ObjectManager.Instance.Player.Position))
                    .FirstOrDefault();
            }
        }

        internal bool NeedToLoot
        {
            get
            {
                var tmp = LootableMob;
                if (tmp != null && BlacklistCurrentLoot)
                {
                    LootBlacklist.Add(tmp.Guid);
                    BlacklistCurrentLoot = false;
                    return false;
                }
                return Inventory.Instance.CountFreeSlots(false) >= GrindSettings.Values.MinFreeSlotsBeforeVendor && tmp != null;
            }
        }

        internal void AddToLootBlacklist(ulong guid)
        {
            if (!LootBlacklist.Contains(guid))
                LootBlacklist.Add(guid);
        }

        internal void RemoveRespawnedMobsFromBlacklist(List<WoWUnit> parList)
        {
            parList.ForEach(i => LootBlacklist.Remove(i.Guid));
        }

        internal void RemoveRespawnedMobsFromBlacklist(ulong parGuid)
        {
            LootBlacklist.Remove(parGuid);
        }
    }
}
